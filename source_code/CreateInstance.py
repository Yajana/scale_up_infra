import boto3
import yaml
import time
import os.path
import sys

dirname, filename = os.path.split(os.path.abspath(__file__))
sg = []
ids = []


def PrintConfig(image, security, instanceNo, instType, region, subnet):
    print("************** Creating Instance **************")
    print("image-id                 :" + image)
    print("security-group-id        :" + security)
    print("number-of-instance       :" + str(instanceNo))
    print("type-of-instance         :" + str(instType))
    print("region                   :" + str(region))
    print("subnet-id                :" + str(subnet))
    print("************************************************")


def PrintResult(result, info):
    print("@@@@@@@@@@@@@@ " + result + " @@@@@@@@@@@@@@@@@@@@")
    print(info)


with open(dirname + '/Input/config.yaml') as stream:
    try:
        content = yaml.load(stream)

        gitVar = content['gitPath']
        gitPath = gitVar[0]

    except BaseException as exe:
        print(exe)


if os.path.isfile(gitPath + "/Input/aws-config.yaml"):
    # print("Reading the Input file...")
    with open(gitPath + "/Input/aws-config.yaml", 'r') as stream:
        try:
            content = yaml.load(stream)
            # print(content)
            ami = content['ami']
            imageVar = ami[0]
            image = imageVar['image-id']

            securityVar = ami[1]
            security = securityVar['security-group-id']

            if 'instance' in content:
                instancevar = content['instance']
                # Creating extra instance for hub
                instanceNo = instancevar[0]

            instanceType = content['instance-type']
            instType = instanceType[0]

            regionVar = content['region']
            region = regionVar[0]

            subnetVar = content['subnet']
            subnet = subnetVar[0]
            
            keynameVar = content['keyname']
            keyname = keynameVar[0]

            # print(image, security, instanceNo)
            sg.append(security)
            # print("Creating Instance with details: ="+str(sg),image, security, instanceNo,instType,region,subnet)
            PrintConfig(image, security, instanceNo, instType, region, subnet)

            try:
                ec2 = boto3.resource('ec2', region_name=region)
                subnet = ec2.Subnet(subnet)
                instances = subnet.create_instances(ImageId=image, InstanceType=instType, MaxCount=instanceNo,
                                                    MinCount=instanceNo,
                                                    KeyName=keyname, SecurityGroups=[], SecurityGroupIds=sg)


            except BaseException as exe:
                PrintResult("Failure", exe)
                # print(exe)
                sys.exit()

            time.sleep(5)
            for instance in instances:
                id = instance.instance_id
                # print("Fetching Id of the instance: ",id)
                ids.append(id)

            # print("Created ID of instances: "+str(ids))

            if os.path.isfile(dirname + "\InstanceId.txt"):
                file = open(dirname + "\InstanceId.txt", "w")
                file.write('\n'.join(str(id) for id in ids))
                file.close()
                # print("Id has been written to file")
                PrintResult("Success", instances)

            else:
                print("InstanceId.txt file is not present in " + str(dirname) + "/aws-config.yaml")
                sys.exit()

        except BaseException as exe:
            print("Problem in Input file 'aws-config.yaml'")
            PrintResult("Failure", exe)
            # print(exe)


else:
    print("Fatal Error: Input file'aws-config.yaml' does not present in " + str(dirname) + "/aws-config.yaml")
    sys.exit()
