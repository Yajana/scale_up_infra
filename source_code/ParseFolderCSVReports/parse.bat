@echo off

cd C:/scale_up_infra/source_code/ParseFolderCSVReports

aws s3 cp s3://scaletests/Dataset/ C:/scale_up_infra/source_code/ParseFolderCSVReports --recursive

::set /p folderpath="Enter Folder path in which all the timestamp files are stored: "
::echo %folderpath%

:: this is to read ip address from report folder to ipaddress.txt
dir C:\scale_up_infra\Reports /b /a-d > ipaddress.txt

::calling ParseAndGenerateGraphs.py for executing parsing script with generating graphs
python ParseAndGenerateGraphs.py
python ZipAFolder.py
::python SendEmail.py
python UpdateDataset.py

del "*.xlsx"
del "*.pyc"
del ipaddress.txt

aws s3 cp dataset.txt  s3://scaletests/Dataset/
aws s3 cp detailset.txt s3://scaletests/Dataset/


