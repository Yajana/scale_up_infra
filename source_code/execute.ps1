Clear-Host
Clear-History
$global:Username = $null
$global:Password = $null



function Credencial($var)
{ 

    foreach($line in Get-Content $env:UserProfile/hosts) {
        #Write-Host $line
        if( $line -like "*$var*"){
            #Write-Host $line.split(' ')
            $ip,$Username,$Password = $line.Split(' ')
            #Write-Host $host,$Username,$Password 

            #$tmp, $Username = $b.Split('=')
            #Write-Host $Username
            SetGlobal $Username Username

            #$tmp, $Password = $c.Split('=')
            #Write-Host $Password
            SetGlobal $Password Password
        }
    }
}

function SetGlobal ($tmp, $varName)
{
   Set-Variable -Name $varName -Value ($tmp) -Scope Global
}

Credencial "windows"
#Write-Host $Username,$Password
$pass = ConvertTo-SecureString -AsPlainText $Password -Force
$Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username,$pass



$ips = @() 

Try
{
    echo "Creating Instances will start now..."
    python C:\scale_up_infra\source_code\CreateInstance.py
}
Catch
{
    echo "Instance not created"
    exit
}

echo "Creating setup...."
Start-Sleep -Seconds 60

python C:\scale_up_infra\source_code\statusCheck.py


$content = Get-Content C:\scale_up_infra\source_code\InstanceId.txt
echo $content

ForEach ($id In $content){
    $instances = aws ec2 describe-instances --instance-ids $id --query 'Reservations[].Instances[].PublicIpAddress' 
    $instance = $instances[1]
    $instance = $instance -replace '"' , ""
    $instance =  $instance.Trim()
    $ips = $ips+$instance
}

Clear-Content 'C:\scale_up_infra\source_code\ipconfig.txt'

ForEach ($i In $ips){
     Write-Host "Client "$i
     $index = [array]::indexof($ips,$i)
     $index = $index+1
     if($ips[$index] -eq $null){
         $i | Add-Content 'C:\scale_up_infra\source_code\ipconfig.txt'
     }
     else{
         $i = $i+","
         $i | Add-Content 'C:\scale_up_infra\source_code\ipconfig.txt'
     }
    
}



#Write-Host "Executing jmeter client on remote system" $ip

ForEach ($ip in $ips){
    Write-Host "Executing slave "$ip
    Start-Job "C:\scale_up_infra\source_code\instance_2.ps1" -ArgumentList $ip
 }
   

Start-Sleep -Seconds 180

Get-job | Receive-Job


cmd.exe /c C:\scale_up_infra\source_code\master.bat



ForEach ($ip In $ips){
    echo -n "Executing on remote system for reports " $ip
    Try
    {
        Invoke-Command -ComputerName $ip -ScriptBlock { cmd.exe /c C:\report.bat } -credential $Cred    
    }
    Catch{

        echo "Unable to connect check the remote connection permission"
    }
}

#$content = Get-Content C:\Distributed-setup\ipconfig.txt

ForEach ($id In $content){
    $instances = aws ec2 describe-instances --instance-ids $id --query 'Reservations[].Instances[].PrivateIpAddress' 
    $instance = $instances[1]
    $instance = $instance -replace '"' , ""
    $i =  $instance.Trim()
    #echo $i
    aws s3 cp  s3://scaletests/Logs/$i C:\scale_up_infra\Reports --recursive
    cd C:\scale_up_infra
    if (Test-Path  C:\scale_up_infra\Reports\jmeter-server.log){
    Rename-Item C:\scale_up_infra\Reports\jmeter-server.log C:\scale_up_infra\Reports\jmeter-server_$i.log
    }
}

echo "Terminating instance"
ForEach ($id In $content){
   aws ec2 terminate-instances --instance-ids $id
}



