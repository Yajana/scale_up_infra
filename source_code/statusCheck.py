import boto3
import os.path
import time

client = boto3.client('ec2')
dirname, filename = os.path.split(os.path.abspath(__file__))
with open(dirname + "/InstanceId.txt") as f:
    ids = f.readlines()
ids = [x.strip() for x in ids]

#print ids
print "-------------------------------------------------"
response = client.describe_instance_status(
    InstanceIds=ids
)
#print response

for i in range(len(ids)):
    try:
        if response["InstanceStatuses"][i]:
            name = response["InstanceStatuses"][i]["InstanceState"]["Name"]
            id = response["InstanceStatuses"][i]["InstanceId"]
            while name != "running":
                print "Instance state " + id + "," + name
                time.sleep(10)
                name = response["InstanceStatuses"][i]["InstanceState"]["Name"]
                response = client.describe_instance_status(InstanceIds=ids)

            print "Instance state " + id + "," + name
            print "-------------------------------------------------"

    except BaseException as exe:
        print "Instance is terminated",exe

for i in range(len(ids)):
    try:
        if response["InstanceStatuses"][i]:
            status = response["InstanceStatuses"][i]["InstanceStatus"]["Status"]
            id = response["InstanceStatuses"][i]["InstanceId"]
            while status != "ok":
                print "Instance Status Checks " + id + "," + status
                time.sleep(10)
                status = response["InstanceStatuses"][i]["InstanceStatus"]["Status"]
                response = client.describe_instance_status(InstanceIds=ids)

        print "Instance Status Checks " + id + "," + status
        print "-------------------------------------------------"

    except BaseException as exe:
        print "Instance is terminated",exe

for i in range(len(ids)):
    try:
        if response["InstanceStatuses"][i]: 
            systemStatus = response["InstanceStatuses"][i]["SystemStatus"]["Status"]
            id = response["InstanceStatuses"][i]["InstanceId"]
            while  systemStatus != "ok" :
                print "System Status Checks " + id + "," + systemStatus
                time.sleep(10)
                systemStatus = response["InstanceStatuses"][i]["SystemStatus"]["Status"]
                response = client.describe_instance_status(InstanceIds=ids)
    
            
            print "System Status Checks " + id + "," + systemStatus
            print "-------------------------------------------------"
    
    except BaseException as exe:
        print "Instance is terminated",exe
    
