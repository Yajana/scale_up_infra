from __future__ import print_function
import yaml
import random
import os
import fileinput
import sys
import subprocess

if len(sys.argv) == 1:
    input_file="/Input/Input.yaml"
else:
    input_file = sys.argv[1]
    print(input_file)

global jmeterPath
global reportPath
global gitPath
chrome_script_path = '/scripts/chrome'

firefox_script_path = '/scripts/firefox'


dirname, filename = os.path.split(os.path.abspath(__file__))
ips = []



def PrintConfig(type,iteration, rampup, concurrency, filepath,timeout,url):
    print("************** "+type+" **************")
    print("jmeter-path              :"+jmeterPath)
    print("script                   :"+filepath)
    print("iteration-in-seconds     :"+str(iteration))
    print("ramp-up-in-seconds:      :"+str(rampup))
    print("concurrency-in-seconds   :"+str(concurrency))
    print("time-out-in-seconds      :"+str(timeout))
    print("url                      :"+url)
    print("systems-info             :"+str(ips))
    print("************************************************")
    print("************** Starting Execution **************")


def jmeter_exection(iteration, rampup, concurrency, filepath,timeout,url):
    global jmeterPath
    os.chdir(jmeterPath)
    os.system("jmeter.bat -n -t"+ filepath+" -r -Gusers="+str(concurrency)+" -Grampup="+str(rampup)+" -Gcount="+str(iteration)+" -Gduration="+str(timeout)+" -GUrl="+str(url))
    print("************** Execution Complete **************")




def read_n_write_ip():
    global jmeterPath
    global gitPath
    with open("ipconfig.txt", 'r') as stream:
        try:
            content = yaml.load(stream)
            ips.append(content)
            print(content)

        except yaml.YAMLError as exc:
            print(exc)

    text = "remote_hosts="
    print(jmeterPath)
    x = fileinput.input(files=jmeterPath+"\jmeter.properties",inplace=1)
    for line in x:
        if text in line:
            line = "remote_hosts="+content+"\n"
        sys.stdout.write(line)
    x.close()



def JMeterTestExecuter(input_file):
    global jmeterPath
    global gitPath
    with open("C:/scale_up_infra/source_code/Input/config.yaml", 'r') as stream:
        try:
            content = yaml.load(stream)

            jmeterVar = content['jmeterPath']
            jmeterPath = jmeterVar[0]

            gitVar = content['gitPath']
            gitPath = gitVar[0]
            #print(gitPath)

            chrome_fullpath = gitPath+chrome_script_path
            firefox_fullpath = gitPath+firefox_script_path


        except yaml.YAMLError as exc:
            print(exc)

    with open(gitPath+input_file, 'r') as stream:
        global concurrency
        concurrency = 1
        iteration = 1
        rampup = 0
        try:
            content = yaml.load(stream)

            read_n_write_ip()

            if 'execution' in content:
                exection = content['execution']
                for var in exection:
                    if "iteration" in var:
                        iteration = var['iteration']
                    elif "concurrency" in var:
                        concurrency = var["concurrency"]
                    elif "ramp-up" in var:
                        rampup = var['ramp-up']

                        if (isinstance(rampup, int)):
                            rampup = int(rampup)

                        elif "m" in rampup:
                            rampup = rampup.replace("m", "")
                            rampup = int(rampup) * 60

                        elif "s" in rampup:
                            rampup = rampup.replace("s", "")
                            rampup = int(rampup)

                #print(concurrency, iteration, rampup)

            if "url" in content:
                urlVar = content['url']
                url = urlVar[0]

            if "time-out" in content:
                timeout = content['time-out'][0]

                if (isinstance(timeout, int)):
                    timeout = int(timeout) * 60

                elif "m" in timeout:
                    timeout = timeout.replace("m", "")
                    timeout = int(timeout) * 60

                elif "s" in rampup:
                    timeout = timeout.replace("s", "")
                    timeout = int(timeout)

            if "random" in content:
                path = content['random'][0]
                lst = os.listdir(path)
                rand = random.randint(0, len(lst) - 1)
                filepath = path + "\\" + lst[rand]
                PrintConfig("Random Execution",iteration, rampup, concurrency, filepath,timeout,url)
                jmeter_exection(iteration, rampup, concurrency, filepath,timeout,url)
            else:
                browsers = content['browsers']
                for browser in browsers:
                    if "chrome" in browser:
                        path = chrome_fullpath

                    elif "firefox" in browser:
                        path = firefox_fullpath

                    scripts = content['scripts']
                    for script in scripts:
                        filedir = path
                        #print(filedir)
                        lst = os.listdir(filedir)
                        if script in lst:
                            filepath = filedir + "\\" + script
                            #print(filepath)
                            PrintConfig("Script Execution",iteration, rampup, concurrency, filepath,timeout,url)
                            jmeter_exection(iteration, rampup, concurrency, filepath,timeout,url)

                        elif os.path.isfile(script):
                            filepath = script
                            PrintConfig("Script Execution"+iteration, rampup, concurrency, filepath,timeout,url)
                            jmeter_exection(iteration, rampup, concurrency, filepath,timeout,url)

                        else:
                            print(script + " script is not present for " + browser)


        except yaml.YAMLError as exc:
            print(exc)

JMeterTestExecuter(input_file)
